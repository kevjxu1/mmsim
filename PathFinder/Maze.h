#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include <cstdio>
#include "Globals.h"

using namespace std;

class PathFinder;

class Maze {
 public:
  Maze(int version, PathFinder *pf);
  void display() const;
  int grid[MAZE_ROWS][MAZE_COLS];
  // grid[i][j] encodes the walls for the cell at position (i, j).

 private:
  PathFinder *pathFinder;

  // maze construction functions
	
  void add_vertical_wall(int r, int c, int size);
  // add vertical wall of specified size starting at (r,c), to the left. 

  void add_horizontal_wall(int r, int c, int size);
  // add horizontal wall of specified size starting at (r,c), to the up. (?)

};

Maze::Maze(int version, PathFinder *pf)
: pathFinder(pf)
{
  int i, j;

  // initialize the grid to 0s. 0 means no wall.
  for (i = 0; i < MAZE_ROWS; i++) {
    for (j = 0; j < MAZE_COLS; j++) {
      grid[i][j] = 0; // 0 indicates no walls
    }
  }
	
  // add walls at the borders.
  add_vertical_wall(0, 0, MAZE_ROWS);
  add_vertical_wall(0, MAZE_COLS, MAZE_ROWS);
  add_horizontal_wall(0, 0, MAZE_COLS);
  add_horizontal_wall(MAZE_ROWS, 0, MAZE_COLS);

  // Draw the selected maze
  switch (version) {
  case VERSION1:
    // picture: http://www.micromouseonline.com/wp/wp-content/uploads/files/cannock-site-images/maze_82us.gif
    add_vertical_wall(0, 1, 7); add_horizontal_wall(1, 2, 5);
    add_vertical_wall(8, 1, 8); add_horizontal_wall(2, 1, 1);
    add_vertical_wall(2, 2, 2); add_horizontal_wall(2, 8, 1);
    add_vertical_wall(5, 2, 2); add_horizontal_wall(2, 12, 1);
    add_vertical_wall(8, 2, 3); add_horizontal_wall(3, 8, 3);
    add_vertical_wall(12, 2, 2); add_horizontal_wall(3, 12, 3);
    add_vertical_wall(2, 3, 5); add_horizontal_wall(4, 8, 3);
    add_vertical_wall(8, 3, 6); add_horizontal_wall(4, 12, 2);
    add_vertical_wall(2, 4, 3); add_horizontal_wall(5, 8, 3);
    add_vertical_wall(6, 4, 4); add_horizontal_wall(5, 12, 2);
    add_vertical_wall(11, 4, 3); add_horizontal_wall(6, 8, 3);
    add_vertical_wall(2, 5, 3); add_horizontal_wall(6, 12, 2);
    add_vertical_wall(6, 5, 4); add_horizontal_wall(7, 1, 1);
    add_vertical_wall(11, 5, 3); add_horizontal_wall(7, 7, 8);
    add_vertical_wall(1, 6, 4); add_horizontal_wall(8, 1, 1);
    add_vertical_wall(6, 6, 4); add_horizontal_wall(8, 9, 5);
    add_vertical_wall(11, 6, 4); add_horizontal_wall(9, 7, 8);
    add_vertical_wall(1, 7, 14); add_horizontal_wall(10, 8, 3);
    add_vertical_wall(0, 8, 1); add_horizontal_wall(10, 12, 2);
    add_vertical_wall(15, 8, 1); add_horizontal_wall(11, 8, 3);
    add_vertical_wall(8, 9, 1); add_horizontal_wall(11, 12, 2);
    add_vertical_wall(1, 10, 2); add_horizontal_wall(12, 8, 3);
    add_vertical_wall(13, 10, 2); add_horizontal_wall(12, 12, 2);
    add_vertical_wall(1, 11, 2); add_horizontal_wall(13, 8, 3);
    add_vertical_wall(13, 11, 2); add_horizontal_wall(13, 12, 3);
    add_vertical_wall(0, 12, 2); add_horizontal_wall(14, 1, 1);
    add_vertical_wall(14, 12, 2); add_horizontal_wall(14, 8, 1);
    add_vertical_wall(1, 13, 1); add_horizontal_wall(14, 12, 1);
    add_vertical_wall(14, 13, 1); add_horizontal_wall(15, 2, 5);
    add_vertical_wall(1, 14, 2); add_horizontal_wall(15, 8, 1);
    add_vertical_wall(13, 14, 2);
    add_vertical_wall(1, 15, 7);
    add_vertical_wall(9, 16, 6);

    break;
  }	
}

void Maze::display() const {
  int i, j;
  for (i = 0; i < MAZE_ROWS; i++) {
    for (j = 0; j < MAZE_COLS; j++) {
      cout << hex << grid[i][j];
    }
    cout << endl;
  }	
}

inline void Maze::add_vertical_wall(int r, int c, int size) {
  int i;
  if (!(r < 0 || r >= MAZE_ROWS || c < 0 || c >= MAZE_COLS)){
    for (i = r; i < r + size; i++) {
      grid[i][c] |= LEFT_WALL;
    }
  }
  // if there are grid cells to the left of the newly constructed wall, update
  // them too. This would only be false if the wall is at the border.
  c--;
  if (!(r < 0 || r >= MAZE_ROWS || c < 0 || c >= MAZE_COLS)){
    if (c >= 0) {
      for (i = r; i < r + size; i++) {
	grid[i][c] |= RIGHT_WALL;
      }
    }
  }
}

inline void Maze::add_horizontal_wall(int r, int c, int size) {
  int j;
  if (!(r < 0 || r >= MAZE_ROWS || c < 0 || c >= MAZE_COLS)){
    for (j = c; j < c + size; j++) {
      grid[r][j] |= UP_WALL;
    }
  }
  // if there are grid cells above the newly constructed wall, update them too.
  r--;
  if (!(r < 0 || r >= MAZE_ROWS || c < 0 || c >= MAZE_COLS)){
    if (r >= 0) {
      for (j = c; j < c + size; j++) {
	grid[r][j] |= DOWN_WALL;
      }
    }
  }
}


#endif // MAZE_H
