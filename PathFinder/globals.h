#ifndef GLOBALS_H
#define GLOBALS_H

// Dimensions of the maze
const int MAZE_ROWS = 16;
const int MAZE_COLS = 16;

// encoding for each grid cell's walls
const int UP_WALL = 0x01; // 00000000 00000000 00000000 00000001
const int DOWN_WALL = 0x02; // 00000000 00000000 00000000 00000010
const int LEFT_WALL = 0x04; // ...00000100
const int RIGHT_WALL = 0x08; // ...00001000

// Which version of the maze do we want to test with?
const int VERSION1 = 1;

enum Dir {
  UP = 0,
  DOWN,
  LEFT,
  RIGHT,
  INVALID
};

struct Coord {
  int row;
  int col;
};

bool coord_compare(Coord c1, Coord c2) {
  return (c1.row == c2.row && c1.col == c2.col);  
}

// for a maze with even dimensions, there will be 4 possible finish cells
const Coord FINISH1 = { MAZE_ROWS/2 - 1, MAZE_COLS/2 - 1 };
const Coord FINISH2 = { MAZE_ROWS/2 - 1, MAZE_COLS/2 };
const Coord FINISH3 = { MAZE_ROWS/2, MAZE_COLS/2 - 1 };
const Coord FINISH4 = { MAZE_ROWS/2, MAZE_COLS/2 };

const Coord START = { MAZE_ROWS - 1, 0 };
// Starting position of agent set at bottom left corner.

#endif // GLOBALS_H
