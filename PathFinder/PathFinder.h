#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "Maze.h"
#include "Globals.h"
#include <cstdlib>
#include <climits>
#include <stack>
#include <list>

using namespace std;

// PathFinder is the maze-solving agent. Its memory of the maze is represented
// by the 2D array, grid.
class PathFinder {
 public:
  PathFinder();
  // initialize each grid cell with 0 walls and compute each cell level
  // assuming no walls exist.
  // Starting position at START.

  ~PathFinder();

  void select_maze(int version);
  // Which maze do we want our mouse to solve?

  void move();
  // Using flood fill algorithm.
  // Check if maze is at finish.
  // Move in the direction of smaller cell level.
  // If movement is not possible, update with Aravind and Bryan's algorithm,
  // then move.

  void find_path();
  // test whether mouse can reach the end of the maze.

  void print_pos() const;
  // print current position of pathfinder
	
  void print_cell_info() const;
  // print the levels of current and neighboring cells.
  // If there is a wall between a neighboring cell, print "WALL" instead.

  const Maze *get_maze() const { return maze; }

  void display_levels() const;

 private:
  struct Cell {
  int level;
  // the most optimistic distance from the finish, based on what the agent  
  // knows the agent begins by assuming there are no walls.

  int walls;
  // encoding for the walls that the mouse knows about
  } grid[MAZE_ROWS][MAZE_COLS];
  
  Coord pos;
  Maze *maze;

  // HELPER FUNCTIONS

  int compute_level(int row, int col);
  // helper function used to compute level of cell, assuming no walls.

  inline Dir select_dir();

  void update_levels();
  
  void update_walls();

};

PathFinder::PathFinder() {

  maze = NULL;
  pos = START;

  // initialize the grid to have no walls
  int i, j;
  for (i = 0; i < MAZE_ROWS; i++) {
    for (j = 0; j < MAZE_COLS; j++) {
      grid[i][j].walls = 0;
    }
  }

  // compute the levels of each cell
  for (i = 0; i < MAZE_ROWS; i++) {
    for (j = 0; j < MAZE_COLS; j++) {
      grid[i][j].level = compute_level(i, j);
    }
  }

}

PathFinder::~PathFinder() {
  if (maze != NULL) {
    delete maze;
    maze = NULL;
  }						   
}

void PathFinder::select_maze(int version) {
  // delete current maze if it exists
  if (maze != NULL) {
    delete maze;
  }
  
  maze = new Maze(version, this);
}

Dir PathFinder::select_dir() {
  Dir best_dir = INVALID;
  int r = pos.row, c = pos.col;
  int curr = grid[r][c].level;

  // select which direction our mouse should go.
  if (!(grid[r][c].walls & UP_WALL) && grid[r - 1][c].level < curr) {
    best_dir = UP;
  }
  else if (!(grid[r][c].walls & DOWN_WALL) && grid[r + 1][c].level < curr) {
    best_dir = DOWN;
  }
  else if (!(grid[r][c].walls & LEFT_WALL) && grid[r][c - 1].level < curr) {
    best_dir = LEFT;
  }
  else if (!(grid[r][c].walls & RIGHT_WALL) && grid[r][c + 1].level < curr) {
    best_dir = RIGHT;
  }
  return best_dir;
}

void PathFinder::update_levels() {
  stack<Coord> s;
  int r = pos.row, c = pos.col;
  Coord to_push = { r, c };
  s.push(to_push);

  Coord cur;
  while (!s.empty()) {
    cur = s.top();

    s.pop();

    r = cur.row;
    c = cur.col;

    int cur_level = grid[r][c].level;

    // keep popping until a neighboring cell needs updating
    // skip the cells from which we have a valid path to move.
    // i.e. a cell does not need updating if there exists a neigboring cell
    // whose level is lower than that of the current cell.

    // skip cells that don't need updating
    if ((!(grid[r][c].walls & UP_WALL) && grid[r - 1][c].level < cur_level)
	|| (!(grid[r][c].walls & DOWN_WALL) && grid[r + 1][c].level < cur_level)
	|| (!(grid[r][c].walls & LEFT_WALL) && grid[r][c - 1].level < cur_level)
	|| (!(grid[r][c].walls & RIGHT_WALL) && grid[r][c + 1].level < cur_level))
	continue;

    // if we get here, current cell needs updating.

    // store current cell's neighbors into a list.
    // neighbors are cells that are not separated by a wall.
    list<Coord> l;
    if (!(grid[r][c].walls & UP_WALL)) {
      Coord to_push = { r - 1, c };
      l.push_back(to_push);
    }
    if (!(grid[r][c].walls & DOWN_WALL)) {
      Coord to_push = { r + 1, c };
      l.push_back(to_push);
    }
    if (!(grid[r][c].walls & LEFT_WALL)) {
      Coord to_push = { r, c - 1 };
      l.push_back(to_push);
    }
    if (!(grid[r][c].walls & RIGHT_WALL)) {
      Coord to_push = { r, c + 1 };
      l.push_back(to_push);
    }

    // get the minimum level in the list of neighbor cells
    list<Coord>::iterator l_iter;
    int min = INT_MAX; // Ideally, should be infinity
    for (l_iter = l.begin(); l_iter != l.end(); l_iter++) {
      Cell temp_cell = grid[(*l_iter).row][(*l_iter).col];
      if (temp_cell.level < min) {
	min = temp_cell.level;
      }
    }

    if (min == INT_MAX) {
      cerr << "ERROR: PathFinder::update_levels failed to get a minimum value\n";
      continue;
    }

    // update current level
    grid[r][c].level = min + 1;

    // push all neighbor cells in the list onto the stack
    for (l_iter = l.begin(); l_iter != l.end(); l_iter++) {
      s.push(*l_iter);
    }

    // remove all elements from the list
    l.clear();

  }
}

void PathFinder::move() {
  // select which direction our mouse should go.
  Dir best_dir = select_dir();

  // if there are no neighboring cells with shorter distance, update grid.
  // Then, select best direction.
  if (best_dir == INVALID) {
    update_levels();

    best_dir = select_dir();
  }

  // update PathFinder position
  switch (best_dir) {
  case UP:
    pos.row--;
    break;
  case DOWN:
    pos.row++;
    break;
  case LEFT:
    pos.col--;
    break;
  case RIGHT:
    pos.col++;
    break;
  default: // INVALID
    cerr << "ERROR: mouse failed to move\n";
  }
}

int PathFinder::compute_level(int row, int col) {

  // compute the distance of the current cell from each of the 4 FINISH cells
  int level1 = abs(row - FINISH1.row) + abs(col - FINISH1.col);
  int level2 = abs(row - FINISH2.row) + abs(col - FINISH2.col);
  int level3 = abs(row - FINISH3.row) + abs(col - FINISH3.col);
  int level4 = abs(row - FINISH4.row) + abs(col - FINISH4.col);

  // return the minimum of the distances
  int min = level1;
  if (level2 < min) {
    min = level2;
  }
  if (level3 < min) {
    min = level3;
  }
  if (level4 < min) {
    min = level4;
  }

  return min;
}

void PathFinder::update_walls() {
  // update current cell's walls  
  grid[pos.row][pos.col].walls |= maze->grid[pos.row][pos.col];
}


void PathFinder::find_path() {
  int n_steps = 0;

  // while pathfinder has not reached the finish
  while (!coord_compare(pos, FINISH1)
	 && !coord_compare(pos, FINISH2)
	 && !coord_compare(pos, FINISH3)
	 && !coord_compare(pos, FINISH4)) {
    
    if (n_steps == 1000000) { // if search takes too long
      cout << "Path is not found\n";
      return;
    }

    update_walls();

    print_pos();
    print_cell_info();

    move();
    n_steps++;
  }

  cout << "PATH IS FOUND!\n";
}

inline void PathFinder::print_cell_info() const{
  int r = pos.row;
  int c = pos.col;
  cout << "level\t";
  cout << "current: " << grid[r][c].level << "    ";

  cout << "up: ";
  if (grid[r][c].walls & UP_WALL) {
    cout << "WALL" << "    ";
  }
  else {
    cout << grid[r - 1][c].level << "    ";
  }

  cout << "down: ";
  if (grid[r][c].walls & DOWN_WALL) {
    cout << "WALL" << "    ";
  }
  else {
    cout << grid[r + 1][c].level << "    ";
  }

  cout << "left: ";
  if (grid[r][c].walls & LEFT_WALL) {
    cout << "WALL" << "    ";
  }
  else {
    cout << grid[r][c - 1].level << "    ";
  }

  cout << "right: ";
  if (grid[r][c].walls & RIGHT_WALL) {
    cout << "WALL" << endl;
  }
  else {
    cout << grid[r][c + 1].level << endl;
  }
}

inline void PathFinder::print_pos() const {
  cout << "r: " << pos.row << "\t" << "c: " << pos.col << endl;
}

void PathFinder::display_levels() const {
  int i, j;
  for (i = 0; i < MAZE_ROWS; i++) {
    for (j = 0; j < MAZE_COLS; j++) {
      cout << grid[i][j].level;
    }
    cout << endl;
  }
}

#endif // PATHFINDER_H
