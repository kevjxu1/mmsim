#include <iostream>
#include <cstdio>
#include "Maze.h"
#include "PathFinder.h"

using namespace std;

int main() {
  PathFinder pf;
  int version = VERSION1;
  pf.select_maze(version);
  /********
    cin >> version;
  switch(version) {
  case VERSION1:
    pf.select_maze(VERSION1);
    break;
  default:
    cout << "ERROR: invalid maze version selected\n";
    }*/
  cout << "Maze version: " << version << endl;
  pf.get_maze()->display();
  cout << endl;
  pf.find_path();
}
